extends "res://engine/velosasi.gd"

var current_weapon

var state_machine_weapon
var state_machine 
var imputVector = Vector2(0,0)
var imputVector2 = Vector2(0,0)

const angle = 0.18 # значение угла для правильного отображения 8-direction анимации

func _ready():
	state_machine = $AnimationTree.get("parameters/playback")
	

func _physics_process(_delta):
	controls_loop()
	if (Input.is_action_just_pressed("ui_dash") || dashTimeCount != 0) && attackTimeCount == 0:
		dash(imputVector)
	if Input.is_action_just_pressed("ui_attack") || attackTimeCount != 0:
		if Input.is_action_just_pressed("ui_attack") && attackTimeCount == 0:
			current_weapon = preload("res://items/sword.tscn").instance()
		return attack_loop(current_weapon)
	movement_loop()
	animation_loop()
	
func attack_loop(current_weapon):
	if imputVector.length() > 0.25:
		move_and_slide(attack(current_weapon, imputVector)/2)
		#animation_loop()
	else:
		attack(current_weapon, imputVector)
	
func controls_loop():
	var LEFT = Input.is_action_pressed("ui_left")
	var RIGHT = Input.is_action_pressed("ui_right")
	var UP = Input.is_action_pressed("ui_up")
	var DOWN = Input.is_action_pressed("ui_down")
	
	imputVector.x = -int(LEFT) + int(RIGHT)
	imputVector.y = -int(UP) + int(DOWN)
	
	#imputVector = Vector2(Input.get_joy_axis(0,JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1))
	#imputVector2 = Vector2(Input.get_joy_axis(0,JOY_AXIS_2), Input.get_joy_axis(0, JOY_AXIS_3))
	imputVector2 = get_local_mouse_position()

func movement_loop():
	if imputVector.length() > 0.25:
		var vect = imputVector.normalized()
		move_and_slide(velosasi(vect))
	else:
		move_and_slide(velosasi(Vector2(0,0)))
		
func animation_loop():
	if imputVector.length() > 0.25:
		if imputVector.x < -angle && imputVector.y < -angle: state_machine.travel("idleupleft")
		elif imputVector.x > angle && imputVector.y < -angle: state_machine.travel("idlerightup")
		elif imputVector.x > angle && imputVector.y > angle: state_machine.travel("idledownright")
		elif imputVector.x < -angle && imputVector.y > angle: state_machine.travel("idledownleft")
		elif imputVector.x < -angle: state_machine.travel("idleleft")
		elif imputVector.x > angle: state_machine.travel("idleright")
		elif imputVector.y > angle: state_machine.travel("walkdown")
		elif imputVector.y < -angle: state_machine.travel("idleup")
	else:
		if imputVector.x < -angle && imputVector.y < -angle: state_machine.travel("idleupleft")
		elif imputVector.x > angle && imputVector.y < -angle: state_machine.travel("idlerightup")
		elif imputVector.x > angle && imputVector.y > angle: state_machine.travel("idledownright")
		elif imputVector.x < -angle && imputVector.y > angle: state_machine.travel("idledownleft")
		elif imputVector.x < -angle: state_machine.travel("idleleft")
		elif imputVector.x > angle: state_machine.travel("idleright")
		elif imputVector.y > angle: state_machine.travel("idledown")
		elif imputVector.y < -angle: state_machine.travel("idleup")

extends KinematicBody2D

var baseSpeed = 1000
var maxSpeed = 200

var imputVector = Vector2(0,0)
var imputVector2 = Vector2(0,0)
#var shapeFactor = 2.0
var weightFactor = 500

var x_imput_gamepad = 0.0
var y_imput_gamepad = 0.0

func _physics_process(delta):
	
	imputVector2 = Vector2(Input.get_joy_axis(0,JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1))
	
	x_imput_gamepad = Input.get_joy_axis(0,JOY_AXIS_0)
	y_imput_gamepad = Input.get_joy_axis(0, JOY_AXIS_1)
	
	#x_imput_gamepad = Input.get_action_strength("ui_gamepad_bomja_right") - Input.get_action_strength("ui_gamepad_bomja_left")
	#y_imput_gamepad = Input.get_action_strength("ui_gamepad_bomja_down") - Input.get_action_strength("ui_gamepad_bomja_up")
	#print(x_imput_gamepad)
	var x_impute = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var y_impute = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")
	
	if abs(x_imput_gamepad) > 0.1: #gamepad velosasi
		imputVector.x += x_imput_gamepad * baseSpeed * delta
		imputVector.x = clamp(imputVector.x, -maxSpeed, maxSpeed)
	else:
		imputVector.x = lerp(imputVector.x, 0, 0.3)
		
	if abs(y_imput_gamepad) > 0.1:
		imputVector.y += y_imput_gamepad * baseSpeed * delta
		imputVector.y = clamp(imputVector.y, -maxSpeed, maxSpeed)
	else:
		imputVector.y = lerp(imputVector.y, 0, 0.3) 

	
	if x_impute != 0: #keyboard velosasi
		imputVector.x += x_impute * baseSpeed * delta
		imputVector.x = clamp(imputVector.x, -maxSpeed, maxSpeed)
	#else:
		#imputVector.x = lerp(imputVector.x, 0, 0.3)
		
	if y_impute != 0:
		imputVector.y += y_impute * baseSpeed * delta
		imputVector.y = clamp(imputVector.y, -maxSpeed / 2, maxSpeed / 2)
	#else:
		#imputVector.y = lerp(imputVector.y, 0, 0.3)
		
	imputVector = move_and_slide(imputVector)
	#print(imputVector)

func getNormalized(var vector, var normalizeTo):
	if vector.length() != 0:
		vector.x *= normalizeTo / vector.length()
		vector.y *= normalizeTo / vector.length()
	else:
		return vector
	return vector

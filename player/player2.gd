extends "res://engine/velosasi.gd"

var imputVector = Vector2(0,0)

func _physics_process(_delta):
	imputVector = Vector2(Input.get_joy_axis(0,JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1))
	if imputVector.length() > 0.25:
		var vect = imputVector.normalized()
		move_and_slide(velosasi(vect))
	else:
		move_and_slide(velosasi(Vector2(0,0)))


extends RigidBody2D

var baseForce = 500
var maxBaseVelocity = 100

var velosasiDo = Vector2(0,0)
var imputVector = Vector2(0,0)
var velosasi = Vector2(0,0)

#var shapeFactor = 2.0
var weightFactor = 500

func _physics_process(delta):
	imputVector = Vector2(Input.get_joy_axis(0,JOY_AXIS_0), Input.get_joy_axis(0, JOY_AXIS_1))
	var vect = imputVector.normalized()
	if imputVector.length() > 0.25:
		add_force(Vector2(0,0), velosasi(vect))
	else:
		add_force(Vector2(0,0), velosasi(vect))





func velosasi(var storedInput):
	var resistanceFactor = -1 * (baseForce/(maxBaseVelocity^2))
	#print(maxBaseVelocity)
	var pushForceVector = getMultipliedBy(storedInput, baseForce)
	var resistanceForce = getMultipliedBy(getNormalized(velosasiDo, getVectorLength(velosasiDo) * getVectorLength(velosasiDo)), resistanceFactor)
	velosasiDo = velosasiDo + getDividedBy(getVectorSum(pushForceVector, resistanceForce), weightFactor)
	#velosasiDo = velosasi
	#print(velosasiDo)
	return velosasiDo




func getMultipliedBy(var vector, var coeff):
	#for i in range(2):
	vector.x *= coeff
	vector.y *= coeff
	return vector

func getNormalized(var vector, var normalizeTo):
	if getVectorLength(vector) != 0:
		#for i in range(2):
		vector.x *= normalizeTo / getVectorLength(vector)
		vector.y *= normalizeTo / getVectorLength(vector)
	else:
		return vector
	return vector

func getDividedBy(var vector , var coeff):
	#for i in range(2):
	vector.x /= coeff
	vector.y /= coeff
	return vector
	
func getVectorSum(var vector1, var vector2):
	var resultVector = Vector2(0,0)
	#for i in range(2):
	resultVector.x = vector2.x + vector1.x
	resultVector.y = vector2.y + vector1.y
	return resultVector

func getVectorLength(var vector):
	var vectorLength = 0.0
	for i in vector:
		vectorLength += i^2
	if vectorLength == 0:
		return 0
	#print(vectorLength)
	return sqrt(vectorLength)

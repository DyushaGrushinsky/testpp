extends Position2D

#onready var parent = $'..'
onready var parent = get_node("../walls/player")

func _ready():
	update_pivot_angle()
	
func _physics_process(delta):
	update_pivot_angle()
	position = Vector2(parent.position)

func update_pivot_angle():
	if parent.imputVector2.length() > 0.25:
		rotation = parent.imputVector2.angle()
		print(position)

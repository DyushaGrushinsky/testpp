extends KinematicBody2D

var baseForce := float(700)
var maxBaseVelocity := float(100)
var resistanceForce = Vector2(0,0)
var velosasi = Vector2(0,0)
var shapeFactor := float(2)
var weightFactor = 50
var resistanceFactor
var pushForceVector = Vector2(0,0)

var phiMax := float(50)
var frictionFactor := float(-0.6)
var g := float(9.8)
var pihl := float(1.5708)
var dashForce := float(50000) #сила толчка

var frictionForce = Vector2(0,0)
var phi := float()

var dashInitial = 4 #длительность фазы инициализации дэша
var dashPush = 1 + dashInitial #если смелый дофига, то трогай >:[
var dashFlight = 7 + dashPush #длительность фазы полета
var dashLanding = 15 + dashFlight #фаза завершения дэша	
var dashTimeCount = 0 #счетчик тиков, в течение которых длится анимация дэша

var attackPrepare = 30 #длительность замаха удара
var attackPrepare2 = attackPrepare * 2 #длительность замаха сильного удара
var attackActive = 4 + attackPrepare2 #длительность атаки
var attackRelise = 15 + attackActive #длительность фозврата в дефолт состояние
var attackTimeCount = 0


enum DashPhase { NONE, INITIAL, PUSH, FLIGHT, LANDING }
var phaseOfDash = DashPhase.NONE

enum attackPhase { NONE, PREPARE, PREPARE2, ACTIVE, RELISE }
var phaseOfAttack = attackPhase.NONE


func attack(var current_weapon, var storedInput):
	print(attackTimeCount)
	if phaseOfAttack == attackPhase.NONE:
		phaseOfAttack = attackPhase.PREPARE
		add_child(current_weapon)
		
	match phaseOfAttack:
		attackPhase.PREPARE:
			current_weapon.state_machine.travel("swingPreapre")
			if Input.is_action_just_pressed("ui_dash") && attackTimeCount < 15: #отмена дэшем
				phaseOfAttack = attackPhase.NONE
				attackTimeCount = 0
				current_weapon.queue_free()
				dash(velosasi)
			elif Input.is_action_just_pressed("ui_accept") && attackTimeCount < 15: #кнопка "отмены"
				phaseOfAttack = attackPhase.NONE
				attackTimeCount = 0
				current_weapon.queue_free()
			if (attackPrepare - 1) == attackTimeCount:
				phaseOfAttack = attackPhase.PREPARE2
			else:
				attackTimeCount += 1
				
		attackPhase.PREPARE2:
			current_weapon.state_machine.travel("swingPrepare2")
			if Input.is_action_pressed("ui_attack"):
				if (attackPrepare2 - 1) != attackTimeCount:
					attackTimeCount += 1
				if Input.is_action_just_pressed("ui_dash"): #отмена дэшем
					phaseOfAttack = attackPhase.NONE
					attackTimeCount = 0
					current_weapon.queue_free()
					dash(velosasi)
				elif Input.is_action_just_pressed("ui_accept"): #кнопка "отмены"
					phaseOfAttack = attackPhase.NONE
					attackTimeCount = 0
					current_weapon.queue_free()
			else:
				attackTimeCount = attackPrepare2 - 1
				phaseOfAttack = attackPhase.ACTIVE
				
		attackPhase.ACTIVE:
			current_weapon.state_machine.travel("swingActive")
			
			if (attackActive - 1) == attackTimeCount:
				phaseOfAttack = attackPhase.RELISE
			else:
				attackTimeCount += 1
			
		attackPhase.RELISE:
			current_weapon.state_machine.travel("swingRelise")
			if Input.is_action_just_pressed("ui_dash") && attackTimeCount > attackRelise-(attackRelise-attackActive)/2: #отмена дэшем
					phaseOfAttack = attackPhase.NONE
					attackTimeCount = 0
					current_weapon.queue_free()
					dash(velosasi)
			if (attackRelise - 1) == attackTimeCount:
				phaseOfAttack = attackPhase.NONE
				current_weapon.queue_free()
			else:
				attackTimeCount += 1
			
	#if phaseOfAttack != attackPhase.NONE: attackTimeCount += 1
	#else: attackTimeCount = 0
	if phaseOfAttack == attackPhase.NONE:
		attackTimeCount = 0
		return velosasi(storedInput)
	if phaseOfAttack == attackPhase.RELISE: return storedInput
	if phaseOfAttack == attackPhase.ACTIVE: return storedInput
	if phaseOfAttack == attackPhase.PREPARE2: return velosasi(storedInput)
	if phaseOfAttack == attackPhase.PREPARE && attackTimeCount < 15:
		return storedInput
	else:
		return velosasi(storedInput)





func dash(var storedInput):
	if phaseOfDash == DashPhase.NONE:
		phaseOfDash = DashPhase.INITIAL
	match phaseOfDash:
		DashPhase.INITIAL:
			if velosasi != Vector2(0,0):
				frictionForce = frictionFactor*g*weightFactor*velosasi.normalized()
				if velosasi.length() > (((getResistanceForce(velosasi) + frictionForce)/weightFactor).length()):
					velosasi += (getResistanceForce(velosasi) + frictionForce)/weightFactor
				else:
					frictionForce = Vector2(0,0)
					resistanceForce = Vector2(0,0)
					velosasi = Vector2(0,0)
			if (dashInitial - 1) == dashTimeCount: phaseOfDash = DashPhase.PUSH
		DashPhase.PUSH:
			velosasi += (getNormalized(storedInput, dashForce) + getResistanceForce(velosasi))/weightFactor
			if (dashPush - 1) == dashTimeCount: phaseOfDash = DashPhase.FLIGHT
		DashPhase.FLIGHT:
			velosasi += getResistanceForce(velosasi)/weightFactor
			if (dashFlight - 1) == dashTimeCount: phaseOfDash = DashPhase.LANDING
		DashPhase.LANDING:
			if velosasi != Vector2(0,0):
				frictionForce = frictionFactor*g*weightFactor*velosasi.normalized()
				if velosasi.length() > (((getResistanceForce(velosasi) + frictionForce)/weightFactor).length()):
					velosasi += (getResistanceForce(velosasi) + frictionForce)/weightFactor
				else:
					frictionForce = Vector2(0,0)
					resistanceForce = Vector2(0,0)
					velosasi = Vector2(0,0)
			if (dashLanding - 1) == dashTimeCount: phaseOfDash = DashPhase.NONE
			
	if phaseOfDash != DashPhase.NONE: dashTimeCount += 1
	else: dashTimeCount = 0
	return velosasi
			

func velosasi(var storedInput):
	resistanceFactor = -(baseForce/(maxBaseVelocity * maxBaseVelocity))
	if !(storedInput == Vector2(0,0)):
		pushForceVector = getNormalized(storedInput, baseForce)
	else:
		pushForceVector = Vector2(0,0)
	if !(pushForceVector == Vector2(0,0) || storedInput == Vector2(0,0)):
		phi = abs(rad2deg(velosasi.angle_to(pushForceVector)))
		if phi != 0:
			frictionForce = frictionFactor * g * weightFactor * storedInput
			if phi <= phiMax: frictionForce *= sin((phi/phiMax)*pihl)
			velosasi += (pushForceVector + getResistanceForce(velosasi) + frictionForce)/weightFactor
			return velosasi
		else:
			velosasi += (pushForceVector + getResistanceForce(velosasi))/weightFactor
			return velosasi
	elif pushForceVector == Vector2(0,0) && velosasi != Vector2(0,0):
		frictionForce = frictionFactor * g * weightFactor * velosasi.normalized()
		if velosasi.length() >= ((getResistanceForce(velosasi) + frictionForce)/weightFactor).length():
			velosasi += (pushForceVector + getResistanceForce(velosasi) + frictionForce)/weightFactor
			return velosasi
		else:
			frictionForce = Vector2(0,0)
			resistanceForce = Vector2(0,0)
			velosasi = Vector2(0,0)
			return velosasi
	if pushForceVector != Vector2(0,0) && velosasi == Vector2(0,0):
		frictionForce = Vector2(0,0)
		velosasi += pushForceVector/weightFactor
		return velosasi
	if pushForceVector == Vector2(0,0) && velosasi == Vector2(0,0):
		return velosasi

func getResistanceForce(var velosasi):
	resistanceForce = resistanceFactor * velosasi.normalized() * velosasi.length_squared()
	if resistanceForce.length() > baseForce:
		resistanceForce = -1 * baseForce * getDecReference(velosasi, maxBaseVelocity)
		return resistanceForce
	return resistanceForce
			
			
func getDecReference(var vector := Vector2(), var referenceLength := float()):
	if vector.length() != 0:
		for i in range(2):
			if vector[i] != 0:
				vector[i] /= referenceLength
	else:
		return Vector2(0,0)
	return vector
			
			
func getAngle(var vector1, var vector2):
	var vectorMulLength = 0.0
	var vectorCosAngle
	for i in range(2):
		vectorMulLength += vector1[i]*vector2[i]
	if(vector1.length()*vector2.length() == 0): return 0
	vectorCosAngle = vectorMulLength/(vector1.length()*vector2.length())
	return rad2deg(acos(vectorCosAngle))

func getNormalized(var vector, var normalizeTo):
	var vectorZap = vector.length()
	if vector.length() != 0:
		if vector.x != 0:
			vector.x *= normalizeTo / vectorZap
		if vector.y != 0:
			vector.y *= normalizeTo / vectorZap
	else:
		return Vector2(0,0)
	return vector

func add_item(item):
	var newitem = item.instance()
	add_child(newitem)

#func free_item(item):
	

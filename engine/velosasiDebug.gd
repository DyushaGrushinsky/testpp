extends Panel

onready var velos = get_parent().get_node("../player")
#onready var veloss = get_parent().get_node("../velosasi")
func _physics_process(_delta):
	$Label7.set_text(String(float(velos.resistanceFactor)))
	$Label8.set_text(String(int(velos.pushForceVector.length())))
	$Label9.set_text(String(int(velos.velosasi.length())))
	$Label10.set_text(String(int(velos.resistanceForce.length())))
	
func _ready():
	$LineEdit.set_text(String(velos.dashInitial))
	$LineEdit2.set_text(String(velos.dashPush))
	$LineEdit3.set_text(String(velos.dashFlight))
	$LineEdit4.set_text(String(velos.dashLanding))

	

func _on_LineEdit_Force_entered(new_text):
	velos.dashInitial = int(new_text)

func _on_LineEdit_Speed_entered(new_text):
	velos.dashPush = int(new_text)

func _on_LineEdit_Weight_entered(new_text):
	velos.dashFlight = int(new_text)

func _on_LineEdit_shapeFactor_entered(new_text):
	velos.dashLanding = int(new_text)

func _on_Force_return():
	velos.dashInitial = 500
	$LineEdit.text = "500"

func _on_Speed_return():
	velos.dashPush = 100
	$LineEdit2.text = "100"

func _on_Weight_return():
	velos.dashFlight = 50
	$LineEdit3.text = "50"

func _on_shapeFactor_returna():
	velos.dashLanding = 2
	$LineEdit4.text = "2"


func _on_Button_pressed():
	$'../velosasiDebug'.set_visible(false)


extends Area2D

export var zIndex:= int()
export var next_floor:= String()

func _physics_process(delta):
	var bodies = get_overlapping_bodies()
	for body in bodies:
		if body.name == "player":
			if body.get_parent().name != next_floor:
				body.z_index = zIndex
				body.set_collision_mask(pow(2, zIndex))
				body.get_parent().remove_child(body)
				get_parent().get_node(next_floor).add_child(body)
